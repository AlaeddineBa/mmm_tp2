package fr.istic.alaeddine.mmm_tp4;

/**
 * Created by alaeddine on 15/02/17.
 */

public class Client {

    private long id;
    private String prenom;
    private String nom;
    private String date;
    private String ville;

    public Client(String prenom, String nom, String date, String ville) {
        this.prenom = prenom;
        this.nom = nom;
        this.date = date;
        this.ville = ville;
    }

    public Client() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }
}
