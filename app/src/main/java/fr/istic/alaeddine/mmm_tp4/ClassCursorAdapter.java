package fr.istic.alaeddine.mmm_tp4;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

/**
 * Created by alaeddine on 07/02/17.
 */

public class ClassCursorAdapter extends CursorAdapter {

    private LayoutInflater mInflater;

    public ClassCursorAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        TextView prenom = (TextView) view.findViewById(R.id.prenom);
        prenom.setText(cursor.getString(cursor.getColumnIndex(Content.PRENOM)));

        TextView nom = (TextView) view.findViewById(R.id.nom);
        nom.setText(cursor.getString(cursor.getColumnIndex(Content.NOM)));

        TextView ville = (TextView) view.findViewById(R.id.ville);
        ville.setText(cursor.getString(cursor.getColumnIndex(Content.VILLE)));

        TextView date = (TextView) view.findViewById(R.id.date);
        date.setText(cursor.getString(cursor.getColumnIndex(Content.DATE)));

    }
}
