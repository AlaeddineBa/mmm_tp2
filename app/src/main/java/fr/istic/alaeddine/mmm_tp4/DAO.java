package fr.istic.alaeddine.mmm_tp4;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class DAO {

	// Database fields
	private SQLiteDatabase database;
	private MySQLiteHelper dbHelper;
	private String[] allColumns = { MySQLiteHelper.ID,
			MySQLiteHelper.PRENOM,
			MySQLiteHelper.NOM,
			MySQLiteHelper.VILLE,
			MySQLiteHelper.DATE };

	public DAO(Context context) {
		dbHelper = new MySQLiteHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public Client createClient(String prenom, String nom, String date, String ville) {
		ContentValues values = new ContentValues();

		values.put(MySQLiteHelper.DATE, ville);
		values.put(MySQLiteHelper.VILLE, date);
		values.put(MySQLiteHelper.NOM, nom);
		values.put(MySQLiteHelper.PRENOM, prenom);

		long insertId = database.insert(MySQLiteHelper.TABLE_CLIENTS, null,
				values);

		System.out.println("Client added with id: " +insertId);

		Cursor cursor = database.query(MySQLiteHelper.TABLE_CLIENTS,
				null, MySQLiteHelper.ID + " = " + insertId, null,
				null, null, null);

		cursor.moveToFirst();
		Client newClient = cursorToClient(cursor);
		cursor.close();
		return newClient;
	}

	public void deleteClient(Client client) {
		long id = client.getId();
		System.out.println("Client deleted with id: " + id);
		database.delete(MySQLiteHelper.TABLE_CLIENTS, MySQLiteHelper.ID
				+ " = " + id, null);
	}

	public List<Client> getAllClients() {
		List<Client> clients = new ArrayList<Client>();

		Cursor cursor = database.query(MySQLiteHelper.TABLE_CLIENTS,
				allColumns, null, null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Client client = cursorToClient(cursor);
			clients.add(client);
			cursor.moveToNext();
		}
		// Make sure to close the cursor
		cursor.close();
		return clients;
	}

	private Client cursorToClient(Cursor cursor) {
		Client client = new Client();
		client.setId(cursor.getLong(0));
		client.setPrenom(cursor.getString(1));
		client.setNom(cursor.getString(2));
		client.setVille(cursor.getString(3));
		client.setDate(cursor.getString(4));
		return client;
	}
}
